"""
module docstring
"""

def compare_number(compare_a, compare_b):
    """
    function docstring
    """
    if not isinstance(compare_a, int) or not isinstance(compare_b, int):
        result = "c est pas un nombre\n"
    elif compare_a == compare_b:
        result = "trouve\n"
    else:
        result = "perdu\n"
    return result


def process_choice(guess_a, guess_b):
    """
    function docstring
    """
    if not isinstance(guess_a, int):
        retour = "c est pas un nombre\n"
        return retour
    if (guess_a < 1) or (guess_a > 5):
        retour = "nombre hors range\n"
        return retour
    retour = compare_number(guess_a, guess_b)
    return retour


if __name__ == '__main__':
    TO_FIND = 2
    choice = input("choisis un nombre")
    print(process_choice(choice, TO_FIND))
