"""
tests pour le programme main
"""


import main


def test_process_choice():
    """
    test fonction process_choice
    """
    assert main.process_choice(1, 2) == main.compare_number(1, 2)
    assert main.process_choice(0, 2) == "nombre hors range\n"
    assert main.process_choice('a', 2) == "c est pas un nombre\n"


def test_compare_number():
    """
    test fonction compare_number
    """
    assert main.compare_number(1, 2) == "perdu\n"
    assert main.compare_number(2, 2) == "trouve\n"
    assert main.compare_number('a', 2) == "c est pas un nombre\n"
    assert main.compare_number(1, 'a') == "c est pas un nombre\n"
    assert main.compare_number('a', 'a') == "c est pas un nombre\n"
